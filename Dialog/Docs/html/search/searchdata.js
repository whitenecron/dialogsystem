var indexSectionsWithContent =
{
  0: "abcdegilmnopqrstv",
  1: "abcdeilmpstv",
  2: "r",
  3: "acdegimoprs",
  4: "p",
  5: "acdinqsv",
  6: "o"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "properties",
  6: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Properties",
  6: "Events"
};

