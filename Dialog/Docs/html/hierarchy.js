var hierarchy =
[
    [ "RPGPack.DialogSystem.IAnswer", "interface_r_p_g_pack_1_1_dialog_system_1_1_i_answer.html", [
      [ "RPGPack.DialogSystem.Answer", "class_r_p_g_pack_1_1_dialog_system_1_1_answer.html", null ]
    ] ],
    [ "RPGPack.DialogSystem.IDialog", "interface_r_p_g_pack_1_1_dialog_system_1_1_i_dialog.html", [
      [ "RPGPack.DialogSystem.Dialog", "class_r_p_g_pack_1_1_dialog_system_1_1_dialog.html", null ]
    ] ],
    [ "RPGPack.UI.IDialogWindow", "interface_r_p_g_pack_1_1_u_i_1_1_i_dialog_window.html", [
      [ "RPGPack.UI.SimpleDialogWindow", "class_r_p_g_pack_1_1_u_i_1_1_simple_dialog_window.html", null ]
    ] ],
    [ "RPGPack.VariableSystem.IInterpretator< T >", "interface_r_p_g_pack_1_1_variable_system_1_1_i_interpretator.html", null ],
    [ "RPGPack.VariableSystem.IInterpretator< bool >", "interface_r_p_g_pack_1_1_variable_system_1_1_i_interpretator.html", [
      [ "RPGPack.VariableSystem.CheckInterpretator", "class_r_p_g_pack_1_1_variable_system_1_1_check_interpretator.html", null ],
      [ "RPGPack.VariableSystem.ExecuteInterpretator", "class_r_p_g_pack_1_1_variable_system_1_1_execute_interpretator.html", null ]
    ] ],
    [ "RPGPack.VariableSystem.IInterpretator< int >", "interface_r_p_g_pack_1_1_variable_system_1_1_i_interpretator.html", [
      [ "RPGPack.VariableSystem.MathInterpretator", "class_r_p_g_pack_1_1_variable_system_1_1_math_interpretator.html", null ]
    ] ],
    [ "RPGPack.Parser.IParser", "interface_r_p_g_pack_1_1_parser_1_1_i_parser.html", [
      [ "RPGPack.Parser.DialogGoogleParser", "class_r_p_g_pack_1_1_parser_1_1_dialog_google_parser.html", null ]
    ] ],
    [ "RPGPack.DialogSystem.IPhrase", "interface_r_p_g_pack_1_1_dialog_system_1_1_i_phrase.html", [
      [ "RPGPack.DialogSystem.Phrase", "class_r_p_g_pack_1_1_dialog_system_1_1_phrase.html", null ]
    ] ],
    [ "RPGPack.DialogSystem.ISpeaker", "interface_r_p_g_pack_1_1_dialog_system_1_1_i_speaker.html", [
      [ "RPGPack.DialogSystem.Speaker", "class_r_p_g_pack_1_1_dialog_system_1_1_speaker.html", null ]
    ] ],
    [ "MonoBehaviour", null, [
      [ "RPGPack.Test.TestPanel", "class_r_p_g_pack_1_1_test_1_1_test_panel.html", null ],
      [ "RPGPack.UI.MainUI", "class_r_p_g_pack_1_1_u_i_1_1_main_u_i.html", null ],
      [ "RPGPack.UI.SimpleDialogWindow", "class_r_p_g_pack_1_1_u_i_1_1_simple_dialog_window.html", null ]
    ] ],
    [ "MonoInstaller", null, [
      [ "RPGPack.Infrastructure.BootstrapInstaller", "class_r_p_g_pack_1_1_infrastructure_1_1_bootstrap_installer.html", null ],
      [ "RPGPack.Infrastructure.LocationInstaller", "class_r_p_g_pack_1_1_infrastructure_1_1_location_installer.html", null ]
    ] ],
    [ "RPGPack.Parser.SpeakerGoogleParser", "class_r_p_g_pack_1_1_parser_1_1_speaker_google_parser.html", null ],
    [ "RPGPack.Tools", "class_r_p_g_pack_1_1_tools.html", null ],
    [ "RPGPack.VariableSystem.VariablesContext", "class_r_p_g_pack_1_1_variable_system_1_1_variables_context.html", [
      [ "RPGPack.VariableSystem.SimpleVariablesContext", "class_r_p_g_pack_1_1_variable_system_1_1_simple_variables_context.html", null ]
    ] ]
];