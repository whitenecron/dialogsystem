var namespace_r_p_g_pack =
[
    [ "DialogSystem", "namespace_r_p_g_pack_1_1_dialog_system.html", "namespace_r_p_g_pack_1_1_dialog_system" ],
    [ "Infrastructure", "namespace_r_p_g_pack_1_1_infrastructure.html", "namespace_r_p_g_pack_1_1_infrastructure" ],
    [ "Parser", "namespace_r_p_g_pack_1_1_parser.html", "namespace_r_p_g_pack_1_1_parser" ],
    [ "Test", "namespace_r_p_g_pack_1_1_test.html", "namespace_r_p_g_pack_1_1_test" ],
    [ "UI", "namespace_r_p_g_pack_1_1_u_i.html", "namespace_r_p_g_pack_1_1_u_i" ],
    [ "VariableSystem", "namespace_r_p_g_pack_1_1_variable_system.html", "namespace_r_p_g_pack_1_1_variable_system" ],
    [ "Tools", "class_r_p_g_pack_1_1_tools.html", null ]
];