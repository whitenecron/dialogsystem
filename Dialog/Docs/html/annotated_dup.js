var annotated_dup =
[
    [ "RPGPack", "namespace_r_p_g_pack.html", [
      [ "DialogSystem", "namespace_r_p_g_pack_1_1_dialog_system.html", [
        [ "Answer", "class_r_p_g_pack_1_1_dialog_system_1_1_answer.html", "class_r_p_g_pack_1_1_dialog_system_1_1_answer" ],
        [ "Dialog", "class_r_p_g_pack_1_1_dialog_system_1_1_dialog.html", "class_r_p_g_pack_1_1_dialog_system_1_1_dialog" ],
        [ "IAnswer", "interface_r_p_g_pack_1_1_dialog_system_1_1_i_answer.html", "interface_r_p_g_pack_1_1_dialog_system_1_1_i_answer" ],
        [ "IDialog", "interface_r_p_g_pack_1_1_dialog_system_1_1_i_dialog.html", "interface_r_p_g_pack_1_1_dialog_system_1_1_i_dialog" ],
        [ "IPhrase", "interface_r_p_g_pack_1_1_dialog_system_1_1_i_phrase.html", "interface_r_p_g_pack_1_1_dialog_system_1_1_i_phrase" ],
        [ "ISpeaker", "interface_r_p_g_pack_1_1_dialog_system_1_1_i_speaker.html", "interface_r_p_g_pack_1_1_dialog_system_1_1_i_speaker" ],
        [ "Phrase", "class_r_p_g_pack_1_1_dialog_system_1_1_phrase.html", "class_r_p_g_pack_1_1_dialog_system_1_1_phrase" ],
        [ "Speaker", "class_r_p_g_pack_1_1_dialog_system_1_1_speaker.html", "class_r_p_g_pack_1_1_dialog_system_1_1_speaker" ]
      ] ],
      [ "Infrastructure", "namespace_r_p_g_pack_1_1_infrastructure.html", [
        [ "BootstrapInstaller", "class_r_p_g_pack_1_1_infrastructure_1_1_bootstrap_installer.html", "class_r_p_g_pack_1_1_infrastructure_1_1_bootstrap_installer" ],
        [ "LocationInstaller", "class_r_p_g_pack_1_1_infrastructure_1_1_location_installer.html", "class_r_p_g_pack_1_1_infrastructure_1_1_location_installer" ]
      ] ],
      [ "Parser", "namespace_r_p_g_pack_1_1_parser.html", [
        [ "DialogGoogleParser", "class_r_p_g_pack_1_1_parser_1_1_dialog_google_parser.html", "class_r_p_g_pack_1_1_parser_1_1_dialog_google_parser" ],
        [ "IParser", "interface_r_p_g_pack_1_1_parser_1_1_i_parser.html", "interface_r_p_g_pack_1_1_parser_1_1_i_parser" ],
        [ "SpeakerGoogleParser", "class_r_p_g_pack_1_1_parser_1_1_speaker_google_parser.html", "class_r_p_g_pack_1_1_parser_1_1_speaker_google_parser" ]
      ] ],
      [ "Test", "namespace_r_p_g_pack_1_1_test.html", [
        [ "TestPanel", "class_r_p_g_pack_1_1_test_1_1_test_panel.html", null ]
      ] ],
      [ "UI", "namespace_r_p_g_pack_1_1_u_i.html", [
        [ "IDialogWindow", "interface_r_p_g_pack_1_1_u_i_1_1_i_dialog_window.html", "interface_r_p_g_pack_1_1_u_i_1_1_i_dialog_window" ],
        [ "MainUI", "class_r_p_g_pack_1_1_u_i_1_1_main_u_i.html", null ],
        [ "SimpleDialogWindow", "class_r_p_g_pack_1_1_u_i_1_1_simple_dialog_window.html", "class_r_p_g_pack_1_1_u_i_1_1_simple_dialog_window" ]
      ] ],
      [ "VariableSystem", "namespace_r_p_g_pack_1_1_variable_system.html", [
        [ "CheckInterpretator", "class_r_p_g_pack_1_1_variable_system_1_1_check_interpretator.html", "class_r_p_g_pack_1_1_variable_system_1_1_check_interpretator" ],
        [ "ExecuteInterpretator", "class_r_p_g_pack_1_1_variable_system_1_1_execute_interpretator.html", "class_r_p_g_pack_1_1_variable_system_1_1_execute_interpretator" ],
        [ "IInterpretator", "interface_r_p_g_pack_1_1_variable_system_1_1_i_interpretator.html", "interface_r_p_g_pack_1_1_variable_system_1_1_i_interpretator" ],
        [ "MathInterpretator", "class_r_p_g_pack_1_1_variable_system_1_1_math_interpretator.html", "class_r_p_g_pack_1_1_variable_system_1_1_math_interpretator" ],
        [ "SimpleVariablesContext", "class_r_p_g_pack_1_1_variable_system_1_1_simple_variables_context.html", "class_r_p_g_pack_1_1_variable_system_1_1_simple_variables_context" ],
        [ "VariablesContext", "class_r_p_g_pack_1_1_variable_system_1_1_variables_context.html", "class_r_p_g_pack_1_1_variable_system_1_1_variables_context" ]
      ] ],
      [ "Tools", "class_r_p_g_pack_1_1_tools.html", null ]
    ] ]
];