using System;

namespace RPGPack.DialogSystem
{
    /// <summary> Player response variant option </summary>
    public interface IAnswer
    {
        /// <summary> Speaker's next phrase if this option is selected </summary>
        public IPhrase nextPhrase { get; set; }
        /// <summary> Show player answer string </summary>
        public string answerOut { get; }
        /// <summary> Set checking the display of this response </summary>
        public void SetCheck(Func<bool> checkAction);
        /// <summary> Check the display of this response </summary>
        public bool CheckAvailable();
        /// <summary> Add action when choosing this option
        /// (usually setting variables) </summary>
        public void AddOnClick(Action onClick);
        /// <summary> Show next phrase and call adding actions
        /// (usually setting variables) </summary>
        public void OnClick();
    }
}
