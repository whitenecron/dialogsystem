using System;
using System.Collections.Generic;
using RPGPack.VariableSystem;

namespace RPGPack.DialogSystem
{
    /// <summary> Dialog system main </summary>
    public interface IDialog
    {
        /// <summary> Will be called when change current phrase </summary>
        public event Action<ISpeaker, IPhrase> OnUpdate;
        /// <summary> All speaker list </summary>
        public List<ISpeaker> speakers { get; }
        /// <summary> Choosed speaker </summary>
        public ISpeaker currSpeaker { get; set; }
        /// <summary> General context variables for the entire dialog </summary>
        public VariablesContext dialogContext { get; }
        /// <summary> Show previously choosed phrase on previously choosed speaker </summary>
        public void Show();
        /// <summary> Show phrase on previously choosed speaker </summary>
        public void Show(IPhrase phrase);
    }
}
