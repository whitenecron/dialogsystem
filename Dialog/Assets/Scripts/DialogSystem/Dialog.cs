using RPGPack.Parser;
using RPGPack.VariableSystem;
using System;
using System.Collections.Generic;

/// <summary> Dialog system for NPCs phrases
/// and player answers </summary>
namespace RPGPack.DialogSystem
{
    /// <summary> Dialog system for character/NPC phrases
    /// and player response variants </summary>
    public class Dialog : IDialog
    {
        /// <summary> Will be called when change current phrase </summary>
        public event Action<ISpeaker, IPhrase> OnUpdate;

        private List<ISpeaker> _speakers = new List<ISpeaker>();
        /// <summary> All speaker list </summary>
        public List<ISpeaker> speakers
        {
            get { return _speakers; }
        }
        /// <summary> Choosed speaker </summary>
        public ISpeaker currSpeaker { get; set; }

        private VariablesContext _dialogContext = new SimpleVariablesContext();
        /// <summary> General context variables for the entire dialog </summary>
        public VariablesContext dialogContext
        {
            get { return _dialogContext; }
        }

        /// <summary> Show previously choosed phrase on previously choosed speaker </summary>
        public void Show()
        {
            Show (currSpeaker.currPhrase);
        }

        /// <summary> Show phrase on previously choosed speaker </summary>
        public void Show(IPhrase phrase)
        {
            if (OnUpdate != null)
                OnUpdate.Invoke(currSpeaker, phrase);
        }

    }
}
