using System;
using Zenject;

namespace RPGPack.DialogSystem
{
    /// <summary> Player response variant option </summary>
    public class Answer : IAnswer
    {
        /// <summary> Speaker's next phrase if this option is selected </summary>
        public IPhrase nextPhrase { get; set; }
        private string _answerStr; // inner player answer string
        /// <summary> Show player answer string </summary>
        public string answerOut
        {
            get { return _answerStr; }
        }
        // dialog system
        private IDialog _dialog;
        private Func<bool> _checkAction;
        private event Action _onClick;

        /// <summary> Create a question with a given text in a given dialog </summary>
        public Answer(string answerStr, IDialog dialog)
        {
            _answerStr = answerStr;
            _dialog = dialog;
        }

        /// <summary> Set checking the display of this response </summary>
        public void SetCheck(Func<bool> checkAction)
        {
            _checkAction = checkAction;
        }

        /// <summary> Check the display of this response </summary>
        public bool CheckAvailable()
        {
            if (_checkAction == null) return true;
            return _checkAction();
        }

        /// <summary> Add action when choosing this option
        /// (usually setting variables) </summary>
        public void AddOnClick(Action onClick)
        {
            _onClick += onClick;
        }

        /// <summary> Show next phrase and call adding actions
        /// (usually setting variables) </summary>
        public void OnClick()
        {
            _onClick?.Invoke();
            if (nextPhrase != null) _dialog.Show(nextPhrase);
        }
    }
}
