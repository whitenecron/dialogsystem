using RPGPack.VariableSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPGPack.DialogSystem
{
    /// <summary> The speaker (character), the set of phrases related to
    /// him and the context of his local variables </summary>
    public interface ISpeaker
    {
        /// <summary> Speaker display name </summary>
        public string nameOut { get; }
        /// <summary> Speaker display image </summary>
        public Sprite icon { get; set; }
        /// <summary> All speaker phrases </summary>
        public List<IPhrase> allPhrases { get; }
        /// <summary> Speaker's current phrase </summary>
        public IPhrase currPhrase { get; set; }
        /// <summary> Speaker's context local variables </summary>
        public VariablesContext variableContext { get; }
    }
}
