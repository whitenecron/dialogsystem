using System.Collections.Generic;

namespace RPGPack.DialogSystem
{
    /// <summary> The speaker (character/NPC) phrase </summary>
    public class Phrase : IPhrase
    {
        // The speaker phrase inner string
        private string _question;
        /// <summary> The speaker phrase show string </summary>
        public string questionOut
        {
            get { return _question; }
        }
        //  Player response options (all variants)
        private List<IAnswer> _answers;

        /// <summary> Create a speaker phrase with given text </summary>
        public Phrase(string questionStr)
        {
            _question = questionStr;
        }

        /// <summary> Set a list of player response options </summary>
        public IPhrase SetAnswers(List<IAnswer> answers)
        {
            _answers = answers;
            return this;
        }

        /// <summary> Get a list of the player's available responses
        /// (with the display condition met) </summary>
        public List<IAnswer> GetAvailableAnswers()
        {
            List<IAnswer> retAnswers = new List<IAnswer>(_answers.Count);
            for (int i = 0; i < _answers.Count; i++)
                if (_answers[i].CheckAvailable())
                    retAnswers.Add(_answers[i]);
            return retAnswers;
        }
    }
}
