using System.Collections.Generic;

namespace RPGPack.DialogSystem
{
    /// <summary> The speaker (character/NPC) phrase </summary>
    public interface IPhrase
    {
        /// <summary> The speaker phrase show string </summary>
        public string questionOut { get; }
        /// <summary> Set a list of player response options </summary>
        public IPhrase SetAnswers(List<IAnswer> answers);
        /// <summary> Get a list of the player's available responses
        /// (with the display condition met) </summary>
        public List<IAnswer> GetAvailableAnswers();
    }
}
