
using RPGPack.VariableSystem;
using System.Collections.Generic;
using UnityEngine;

namespace RPGPack.DialogSystem
{
    /// <summary> The speaker (character/NPC), the set of phrases related to
    /// him and the context of his local variables </summary>
    public class Speaker : ISpeaker
    {
        // The speaker inner name
        private string _name;
        /// <summary> Speaker display name </summary>
        public string nameOut
        {
            get { return _name; }
        }
        /// <summary> Speaker display image </summary>
        public Sprite icon { get; set; }
        private List<IPhrase> _phrases = new List<IPhrase>();
        /// <summary> All speaker phrases </summary>
        public List<IPhrase> allPhrases
        {
            get { return _phrases; }
        }
        /// <summary> Speaker's current phrase </summary>
        public IPhrase currPhrase { get; set; }
        /// <summary> Speaker's context local variables </summary>
        public VariablesContext variableContext { get; private set; }

        /// <summary> Create a speaker with the given name, include the variable context
        /// in the specified context (the context of the general dialog variables) </summary>
        public Speaker(string name, VariablesContext parentVarContext = null)
        {
            _name = name;
            variableContext = new SimpleVariablesContext(parentVarContext);
        }
    }
}
