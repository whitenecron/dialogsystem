using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPGPack
{
    /// <summary>
    /// Small utility functions
    /// </summary>
    public class Tools
    {
        /// <summary> Create full copy GameObject </summary>
        public static GameObject GetCopyGO(GameObject go)
        {
            Transform ret = MonoBehaviour.Instantiate(go).transform;
            ret.SetParent(go.transform.parent);
            ret.position = go.transform.position;
            ret.rotation = go.transform.rotation;
            ret.localScale = go.transform.localScale;
            return ret.gameObject;
        }
    }
}