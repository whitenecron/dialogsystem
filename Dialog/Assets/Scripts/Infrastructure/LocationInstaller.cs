using RPGPack.DialogSystem;
using RPGPack.Parser;
using UnityEngine;
using Zenject;

namespace RPGPack.Infrastructure
{
    /// <summary>
    /// Bind objects in load scene
    /// </summary>
    public class LocationInstaller : MonoInstaller
    {
        /// <summary>
        /// Bind objects in load scene
        /// </summary>
        public override void InstallBindings()
        {
            BindDialog();
        }

        private void BindDialog()
        {
            IDialog dialog = new Dialog();
            Container.Bind<IDialog>().FromInstance(dialog).AsSingle();
            IParser parser = new DialogGoogleParser(dialog);
            parser.OnFinishParse += () =>
            {
                if (dialog.speakers.Count > 0)
                {
                    dialog.currSpeaker = dialog.speakers[0];
                    dialog.Show();
                }
            };
            //Container.Bind<IParser>().FromInstance(parser).AsSingle();
        }
    }
}