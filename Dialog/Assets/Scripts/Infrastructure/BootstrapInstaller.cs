using Zenject;

/// <summary>Launching the application and binding
/// links between systems </summary>
namespace RPGPack.Infrastructure
{
    /// <summary>
    /// Bind objects in start application
    /// </summary>
    public class BootstrapInstaller : MonoInstaller
    {
        /// <summary>
        /// Bind objects in start application
        /// </summary>
        public override void InstallBindings()
        {

        }
    }
}