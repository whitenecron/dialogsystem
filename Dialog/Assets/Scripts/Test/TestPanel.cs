using System.Collections;
using System.Collections.Generic;
using RPGPack.VariableSystem;
using UnityEngine;
using UnityEngine.UI;

/// <summary> Test apllication </summary>
namespace RPGPack.Test
{
    /// <summary>
    /// Test variable contexts and interpretators panel
    /// </summary>
    public class TestPanel : MonoBehaviour
    {

        public InputField checkInput;
        public Text resultTxt;
        public InputField executeInput;
        private VariablesContext _context = new SimpleVariablesContext();

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.LeftControl))
                resultTxt.text = "" + _context.Check(checkInput.text);
            if (Input.GetKeyDown(KeyCode.Tab))
                _context.Execute(executeInput.text);
        }
    }
}