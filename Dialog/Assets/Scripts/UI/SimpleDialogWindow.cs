using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RPGPack.DialogSystem;
using RPGPack.Parser;
using Zenject;

namespace RPGPack.UI
{
    /// <summary>
    /// Displaying a phrase from the dialog system showing answer options
    /// </summary>
    public class SimpleDialogWindow : MonoBehaviour, IDialogWindow
    {
        public RectTransform _container;
        public Text speakerNameText;
        public Text quessionText;
        public RectTransform answerSample;
        public Image persIcon;
        public Image defaultIcon;

        private IDialog _dialog;
        private List<GameObject> _answerRectGOs = new List<GameObject>();
        private List<Text> _answerTexts = new List<Text>();
        private List<IAnswer> _currAnswerList;

        /// <summary> Bind to dialog system </summary>
        /// <param name="dialog">  Dialog system </param>
        [Inject]
        private void Construct(IDialog dialog)
        {
            Debug.Log("SimpleDialogWindow.Construct");
            _dialog = dialog;
            _dialog.OnUpdate += Show;
        }

        /// <summary> Displaying a phrase from the dialog system </summary>
        /// <param name="speaker"> NPC speaker </param>
        /// <param name="phrase"> phrase</param>
        public void Show(ISpeaker speaker, IPhrase phrase)
        {
            _container.gameObject.SetActive(true);
            speakerNameText.text = speaker.nameOut;
            quessionText.text = phrase.questionOut;
            Sprite sprite = _dialog.currSpeaker.icon;
            persIcon.sprite = sprite;
            persIcon.enabled = sprite != null;
            defaultIcon.enabled = sprite == null;
            _currAnswerList = phrase.GetAvailableAnswers();
            for (int i = 0; i < _currAnswerList.Count; i++)
            {
                Text currText;
                if (i < _answerTexts.Count)
                    currText = _answerTexts[i];
                else
                {
                    GameObject answerRectGO = Tools.GetCopyGO(answerSample.gameObject);
                    _answerRectGOs.Add(answerRectGO);
                    int _i = i;
                    answerRectGO.GetComponent<Button>().onClick.AddListener(() => OnClickBtn(_i));
                    currText = answerRectGO.transform.GetChild(0).GetComponent<Text>();
                    _answerTexts.Add(currText);
                }
                currText.text = _currAnswerList[i].answerOut;
            }
            for (int i = 0; i < _answerRectGOs.Count; i++)
                _answerRectGOs[i].SetActive(i < _currAnswerList.Count);
        }

        // click on answer btn
        private void OnClickBtn(int index)
        {
            _currAnswerList[index].OnClick();
        }

        void Awake()
        {
            _container.gameObject.SetActive(false);
            answerSample.gameObject.SetActive(false);
        }
    }
}
