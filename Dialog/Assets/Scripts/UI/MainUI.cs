using System.Collections;
using System.Collections.Generic;
using RPGPack.Parser;
using UnityEngine;
using UnityEngine.UI;

/// <summary> GUI elements </summary>
namespace RPGPack.UI
{
    /// <summary>
    /// UI application controls not included in individual systems (close, change source, help)
    /// </summary>
    public class MainUI : MonoBehaviour
    {
        public Button exitBtn;
        public InputField sourseInput;
        public Button changeSourceBtn;
        public Button helpBtn;

        // Start is called before the first frame update
        void Start()
        {
            exitBtn.onClick.AddListener(() => { Application.Quit(); });
            changeSourceBtn.onClick.AddListener(() => DialogGoogleParser.OnChangeSource(sourseInput.text));
            helpBtn.onClick.AddListener(() => Application.OpenURL("https://docs.google.com/document/d/1qwxLgCfZHsh69Jf72Yq5dQP_28hhUpq7viF2lmvqzNo/edit?usp=sharing"));
        }
    }
}
