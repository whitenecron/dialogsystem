using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPGPack.DialogSystem;

namespace RPGPack.UI
{
    /// <summary>
    /// Displaying a phrase from the dialog system showing answer options
    /// </summary>
    public interface IDialogWindow
    {
        /// <summary> Displaying a phrase from the dialog system </summary>
        /// <param name="speaker"> NPC speaker </param>
        /// /// <param name="phrase"> phrase</param>
        public void Show(ISpeaker speaker, IPhrase phrase);

    }
}
