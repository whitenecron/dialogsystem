using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using GoogleSheetsToUnity;
using RPGPack.DialogSystem;
using UnityEngine;
using Zenject;

namespace RPGPack.Parser
{
    /// <summary>
    /// Load and parsing data from google sheets (dialogs and variables)
    /// </summary>
    public class DialogGoogleParser : IParser
    {
        private static string _tableId = "1MgNLA5FsvB3tJQCi7u0Cn-PHHmBJO1ZJe18DMAAARZE";

        public static string tableId
        {
            get { return _tableId; }
        }

        /// <summary> Load data from google sheet from url and full refresh  </summary>
        /// <param name="source"> Full URL </param>
        public static void OnChangeSource(string source)
        {
            try
            {
                _tableId = source.Split('/')[5];
                Application.LoadLevel(Application.loadedLevel);
            }
            catch
            {
                Debug.LogError("No correct source URL, use google spreadsheets");
            }
        }

        /// <summary> Will be called when parsing is completed </summary>
        public event Action OnFinishParse;

        /// <summary> Dialog system </summary>
        public IDialog coinnectedDialog { get; private set; }


        private const string personageSheet = "Personages";
        private const string variablesSheet = "Variables";
        private const int startRowIndex = 2;
        private int _parsingSpeakersCount = 0; // start speaker parsing count
        private int _readySpeakersCount = 0; // complete speaker parsing count
        private List<SpeakerGoogleParser> _speakerParsers = new List<SpeakerGoogleParser>();

        /// <summary> Init and start parser </summary>
        /// <param name="dialog">Dialog system</param>
        public DialogGoogleParser(IDialog dialog)
        {
            Debug.Log("Parser.Construct");
            coinnectedDialog = dialog;
            Parse();
        }

        /// <summary>
        /// Start parsing process, calling to load personage list and global dialog variables list
        /// </summary>
        public void Parse()
        {
            SpreadsheetManager.ReadPublicSpreadsheet(new GSTU_Search(tableId, personageSheet), OnLoadPersonageSheet);
            SpreadsheetManager.ReadPublicSpreadsheet(new GSTU_Search(tableId, variablesSheet), OnLoadVariablesSheet);
        }

        /// <summary> Registration of the connection between the answer and the subsequent
        /// question in case of choosing another speaker </summary>
        /// <param name="currAnswer">the answer from which the transition will be made</param>
        /// <param name="speakerName">target speaker name</param>
        /// <param name="indexNextPhrase">line in sheet with target phrase</param>
        public void SetAnswerNextPhraseId(IAnswer currAnswer, string speakerName, int indexNextPhrase)
        {
            for (int i = 0; i < _speakerParsers.Count; i++)
            {
                if (_speakerParsers[i].speakerName == speakerName)
                    _speakerParsers[i].SetAnswerNextPhraseId(currAnswer, indexNextPhrase, true);
            }
        }

        // Parsing a list of characters and calling to load sheets with their phrases
        private void OnLoadPersonageSheet(GstuSpreadSheet speakerListSheet)
        {
            int indexRow = startRowIndex;
            while (speakerListSheet.rows.ContainsKey(indexRow))
            {
                string speakerName = speakerListSheet.rows[indexRow][0].value;
                Debug.Log("speakerName    " + speakerName);
                SpeakerGoogleParser speakerParse =
                    new SpeakerGoogleParser(this, speakerName, OnFinishParseOnePersonageSheet);
                _speakerParsers.Add(speakerParse);
                SpreadsheetManager.ReadPublicSpreadsheet(new GSTU_Search(tableId, speakerName),
                    speakerParse.ParseOneSpeaker);
                int _indexRow = indexRow;
                LoadSpeakerSprite(speakerListSheet, speakerParse, _indexRow);
                indexRow++;
            }

            _parsingSpeakersCount = indexRow - startRowIndex;
        }

        // Uploading an image from the link and setting it as a speaker icon
        private async void LoadSpeakerSprite(GstuSpreadSheet speakerListSheet, SpeakerGoogleParser speakerParse,
            int indexRow)
        {
            if (speakerListSheet.rows[indexRow].Count < 2)
                return;
            string iconURL = speakerListSheet.rows[indexRow][1].value;
            if (iconURL.ToLower() != "null" && iconURL.Length > 0)
            {
                WWW www = new WWW(iconURL);
                while (!www.isDone)
                    await Task.Delay(10);
                Sprite sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height),
                    new Vector2(0.5f, 0.5f));
                speakerParse.icon = sprite;
            }
        }

        // Call on completion of each speaker's parsing
        private void OnFinishParseOnePersonageSheet(SpeakerGoogleParser speakerParser)
        {
            _readySpeakersCount++;
            Debug.Log(_readySpeakersCount + "___" + _parsingSpeakersCount);
            if (_readySpeakersCount == _parsingSpeakersCount)
            {
                FinishParseSpeakers();
            }
        }

        // Init dialog system after all speakers phrases parsing
        private void FinishParseSpeakers()
        {
            for (int i = 0; i < _speakerParsers.Count; i++)
            {
                if (_speakerParsers[i].speaker != null)
                    coinnectedDialog.speakers.Add(_speakerParsers[i].speaker);
                else
                    Debug.LogError("Speaker " + _speakerParsers[i].speakerName + " parsing error");
                _speakerParsers[i].SetNextPhraseLinks();
            }

            OnFinishParse?.Invoke();
        }

        // Parsing a list of global dialog variables
        private void OnLoadVariablesSheet(GstuSpreadSheet speakerListSheet)
        {
            int indexRow = startRowIndex;
            while (speakerListSheet.rows.ContainsKey(indexRow))
            {
                string varName = speakerListSheet.rows[indexRow][0].value;
                int varValue = 0;
                int.TryParse(speakerListSheet.rows[indexRow][1].value, out varValue);
                coinnectedDialog.dialogContext.SetValue(varName, varValue);
                indexRow++;
            }
        }
    }
}
