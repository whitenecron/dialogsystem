using RPGPack.DialogSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary> Load and parsing data </summary>
namespace RPGPack.Parser
{
    /// <summary> Load and parsing data </summary>
    public interface IParser
    {
        /// <summary> Will be called when parsing is completed </summary>
        public event Action OnFinishParse;
        /// <summary> Dialog system </summary>
        public IDialog coinnectedDialog { get; }
        /// <summary> Start parsing process, calling to load personage
        /// list and global variables list </summary>
        public void Parse();
    }
}
