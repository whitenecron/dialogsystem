using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using GoogleSheetsToUnity;
using RPGPack.DialogSystem;
using UnityEngine;


namespace RPGPack.Parser
{
    /// <summary> Load and parsing one speaker data from
    /// google sheets (dialogs and variables) </summary>
    public class SpeakerGoogleParser
    {
        public string speakerName { get; }
        /// result speaker
        public Speaker speaker { get; private set; }
        public Sprite _icon;
        public Sprite icon
        {
            get { return _icon; }
            set
            {
                _icon = value;
                if (speaker != null)
                    speaker.icon = value;
            } }

        private const int startRowIndex = 2;
        private DialogGoogleParser _mainParser;
        private Action<SpeakerGoogleParser> _onFinishParse;
        private Dictionary<int, IPhrase> _indexToPhrase = new Dictionary<int, IPhrase>();
        private Dictionary<IAnswer, int> _answerToNextLink = new Dictionary<IAnswer, int>();

        
        public SpeakerGoogleParser(DialogGoogleParser mainParser, string speakerName, Action<SpeakerGoogleParser> onFinishParse)
        {
            _mainParser = mainParser;
            this.speakerName = speakerName;
            _onFinishParse = onFinishParse;
            speaker = new Speaker(speakerName, _mainParser.coinnectedDialog.dialogContext);
        }

        public void ParseOneSpeaker(GstuSpreadSheet speakerListSheet)
        {
            Debug.Log("ParseOneSpeaker    " + speakerName);
            
            int indexRow = startRowIndex;
            try
            {
                IPhrase currPhrase = null;
                List<IAnswer> answers = new List<IAnswer>(7);
                while (speakerListSheet.rows.ContainsKey(indexRow))
                {
                    string strValue = speakerListSheet.rows[indexRow][0].value;
                    if (strValue[0] == 'q')
                    {
                        FinishParsePhrase(currPhrase, answers);
                        currPhrase = ParsePhrase(speakerListSheet.rows[indexRow]);
                        answers = new List<IAnswer>();
                        speaker.allPhrases.Add(currPhrase);
                        _indexToPhrase.Add(indexRow, currPhrase);
                    }
                    else
                    {
                        IAnswer currAnswer = ParseAnswer(speakerListSheet.rows[indexRow]);
                        answers.Add(currAnswer);
                    }
                    indexRow++;
                }
                FinishParsePhrase(currPhrase, answers);
            }
            catch
            {
                Debug.LogError("No correct parse speaker " + speakerName + " line: " + indexRow);
            }
            finally
            {
                if (speaker.allPhrases.Count > 0)
                {
                    speaker.currPhrase = speaker.allPhrases[0];
                }
                _onFinishParse?.Invoke(this);
            }
        }

        public void SetAnswerNextPhraseId(IAnswer currAnswer, int indexNextPhrase, bool isChangeSpeaker = false)
        {
            _answerToNextLink.Add(currAnswer, indexNextPhrase);
            if (isChangeSpeaker)
                currAnswer.AddOnClick(() => _mainParser.coinnectedDialog.currSpeaker = speaker);
        }

        public void SetNextPhraseLinks()
        {
            foreach (var currAnswer in _answerToNextLink)
                if (currAnswer.Value > 0 && _indexToPhrase.ContainsKey(currAnswer.Value))
                {
                    currAnswer.Key.nextPhrase = _indexToPhrase[currAnswer.Value];
                }
        }

        private IPhrase ParsePhrase(List<GSTU_Cell> cells)
        {
            string phraseStr = cells[1].value;
            return new Phrase(phraseStr);
        }
        private void FinishParsePhrase(IPhrase phrase, List<IAnswer> answers)
        {
            if (phrase != null)
                phrase.SetAnswers(answers);
        }

        private IAnswer ParseAnswer(List<GSTU_Cell> cells)
        {
            string answerStr = cells[1].value;
            IAnswer answer = new Answer(answerStr, _mainParser.coinnectedDialog);
            string nextPhrase = cells.Count > 3 ? cells[3].value : "";
            if (nextPhrase.Length > 1 && nextPhrase[0] == '=')
            {
                int indexNextPhrase = int.Parse(Regex.Replace(nextPhrase, "[^0-9]", ""));
                int n1 = nextPhrase.IndexOf('\'');
                int n2 = nextPhrase.LastIndexOf('\'');
                if (n1 >= 0 && n2 > n1)
                {
                    string listName = nextPhrase.Substring(n1 + 1, n2 - n1 - 1);
                    _mainParser.SetAnswerNextPhraseId(answer, listName, indexNextPhrase);
                }
                else
                    SetAnswerNextPhraseId(answer, indexNextPhrase);
            }
            string CheckExecute = cells.Count > 4 ? cells[4].value : "";
            answer.AddOnClick(() => { speaker.variableContext.Execute(CheckExecute); });
            string CheckAvailable = cells.Count > 5 ? cells[5].value : "";
            answer.SetCheck(() => { return speaker.variableContext.Check(CheckAvailable); });
            return answer;
        }
    }
}
