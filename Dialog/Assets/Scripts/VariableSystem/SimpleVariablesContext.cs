using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace RPGPack.VariableSystem
{

    /// <summary>
    /// Storage of variables of a certain context with the main actions for working with them.
    /// Including interpretators. Simple variant (MathInterpretator, ExecuteInterpretator, CheckInterpretator).
    /// </summary>
    public class SimpleVariablesContext : VariablesContext
    {
        /// <summary></summary>
        /// <param name="parent"> parent context (used if variables not found in current context)</param>
        public SimpleVariablesContext(VariablesContext parent = null)
        {
            _mathfInterpretator = new MathInterpretator(this);
            _executeInterpretator = new ExecuteInterpretator(this, _mathfInterpretator);
            _checkInterpretator = new CheckInterpretator(_mathfInterpretator);
            parentContext = parent;
        }
    }
}
