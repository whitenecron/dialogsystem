using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RPGPack.VariableSystem
{

    /// <summary>
    /// Determining the value of a complex boolean expression.
    /// Based on math interpreter (MathInterpretator).
    /// Supported operators: '>', '<', '=', '!', '>=', '<=', '!='('<>'), '&', '|'
    /// </summary>
    public class CheckInterpretator : IInterpretator<bool>
    {
        private readonly char[] unitCompareOperators = new char[] { '&', '|' };
        private readonly char[] compareOperators = new char[] { '>', '<', '=', '!' };
        private IInterpretator<int> _mathInterpretator;
        /// <summary></summary>
        /// <param name="mathInterpretator">  Interpreter for algebraic expressions  </param>
        public CheckInterpretator(IInterpretator<int> mathInterpretator)
        {
            _mathInterpretator = mathInterpretator;
        }

        /// <summary>
        /// Calculate block multiple comparisons to bool result
        /// </summary><returns>comparisons result</returns>
        public bool Run(string block)
        {
            try
            {
                return Check(block);
            }
            catch
            {
                Debug.LogError("No correct check block: " + block);
                return true;
            }
        }

        // combining multiple comparisons (recursive for brackets)
        private bool Check(string condition)
        {
            List<char> operators = new List<char>();
            List<bool> operands = new List<bool>();
            int deepBracket = 0;
            int lastOperatorSymbolIndex = -1;
            bool isBracketInCurrent = false;
            for (int i = 0; i < condition.Length; i++)
            {
                if (condition[i] == '(')
                {
                    deepBracket++;
                    isBracketInCurrent = true;
                }
                else if (condition[i] == ')')
                {
                    deepBracket--;
                }

                if (deepBracket == 0 && unitCompareOperators.Contains(condition[i]))
                {
                    if (i > 0 && condition[i] != condition[i - 1])
                    {
                        operators.Add(condition[i]);

                        if (isBracketInCurrent)
                        {
                            operands.Add(Check(condition.Substring(lastOperatorSymbolIndex + 1,
                                i - lastOperatorSymbolIndex - 1)));
                            isBracketInCurrent = false;
                        }
                        else
                            operands.Add(OneCheck(condition.Substring(lastOperatorSymbolIndex + 1,
                                i - lastOperatorSymbolIndex - 1)));
                    }
                    lastOperatorSymbolIndex = i;
                }

            }
            operands.Add(OneCheck(condition.Substring(lastOperatorSymbolIndex + 1)));

            for (int i = operators.Count - 1; i >= 0; i--)
            {
                if (operators[i] == '&')
                {
                    operands[i] &= operands[i + 1];
                    operators.RemoveAt(i);
                    operands.RemoveAt(i + 1);
                }
            }

            bool retValue = false;
            for (int i = 0; i < operands.Count; i++)
            {
                retValue |= operands[i];
            }
            // TODO for debug
            /*string str = operands[0] + "";
            for (int i = 0; i < operators.Count; i++)
            {
                str += operators[i] + "" + operands[i + 1];
            }
            Debug.Log(str);*/

            return retValue;
        }

        // Calculate simple compare
        private bool OneCheck(string condition)
        {
            int comparePos = condition.IndexOfAny(compareOperators);
            if (comparePos < 0)
            { 
                return condition.ToLower().Contains("true");
            }

            int copareLength = compareOperators.Contains(condition[comparePos + 1]) ? 2 : 1;
            int leftValue = _mathInterpretator.Run(condition.Substring(0, comparePos));
            int rightValue = _mathInterpretator.Run(condition.Substring(comparePos + copareLength));
            switch (condition[comparePos])
            {
                case '>':
                    if (copareLength > 1 && condition[comparePos + 1] == '=') return leftValue >= rightValue;
                    else if (copareLength > 1 && condition[comparePos + 1] == '<') return leftValue != rightValue;
                    else return leftValue > rightValue;
                case '<':
                    if (copareLength > 1 && condition[comparePos + 1] == '=') return leftValue <= rightValue;
                    else if (copareLength > 1 && condition[comparePos + 1] == '>') return leftValue != rightValue;
                    else return leftValue < rightValue;
                case '=':
                    if (copareLength > 1 && condition[comparePos + 1] == '>') return leftValue >= rightValue;
                    else if (copareLength > 1 && condition[comparePos + 1] == '<') return leftValue <= rightValue;
                    if (copareLength > 1 && condition[comparePos + 1] == '!') return leftValue != rightValue;
                    else return leftValue == rightValue;
                case '!':
                    return leftValue != rightValue;

            }
            return leftValue == rightValue;
        }
    }
}
