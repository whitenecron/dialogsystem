using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RPGPack.VariableSystem
{

    /// <summary>
    /// Calculation of arithmetic expressions, int numbers and named int variables are supported, the values of which are substituted from the current context.
    /// Supported operators: '+', '-', '*', '/', '%', '(', ')'
    /// </summary>
    public class MathInterpretator : IInterpretator<int>
    {

        private readonly char[] mathfOperators = new char[] { '+', '-', '*', '/', '%', '(', ')' };
        private VariablesContext _context;
        /// <summary></summary>
        /// <param name="context"> Context for getting variables </param>
        public MathInterpretator(VariablesContext context)
        {
            _context = context;
        }

        /// <param name="formula">  Arithmetic formula </param>
        /// <returns>result formula</returns>
        public int Run(string formula)
        {
            try
            {

                List<char> operators = new List<char>();
                List<int> operands = new List<int>();
                bool isCurrentNumber = false; // character-by-character reading of a number
                string currentVar = ""; // character-by-character reading of a variable
                bool isMayMinus = true; // if there is a "-" whether to consider it unary
                bool isMinus = false; // negation of the current operand
                for (int i = 0; i < formula.Length; i++)
                {
                    bool isOperator = mathfOperators.Contains(formula[i]);
                    if (isOperator)
                    {
                        if (formula[i] == '-' && isMayMinus)
                            isMinus = true;
                        else
                        {
                            operators.Add(formula[i]);
                            isMayMinus = (formula[i] == '(');
                            if (currentVar.Length > 0)
                            {
                                operands.Add((isMinus ? -1 : 1) * _context.GetValue(currentVar));
                                currentVar = "";
                            }

                            isCurrentNumber = false;
                        }
                    }
                    else if (formula[i] >= '0' && formula[i] <= '9')  // character-by-character collection of a number
                    {
                        isMayMinus = false;
                        int currSymbol = formula[i] - '0';
                        if (!isCurrentNumber)
                        {
                            operands.Add((isMinus ? -1 : 1) * currSymbol);
                        }
                        else
                            operands[operands.Count - 1] =
                                operands[operands.Count - 1] * 10 + (isMinus ? -1 : 1) * currSymbol;

                        isCurrentNumber = true;
                    }
                    else if (formula[i] != ' ') // character-by-character collection of a variable
                    {
                        isMayMinus = false;
                        currentVar += formula[i];
                    }
                }

                if (currentVar.Length > 0)
                    operands.Add(_context.GetValue(currentVar));

                return BracketsWrapper(operators, operands);
            }
            catch
            {
                Debug.LogError("No correct formula: " + formula);
                return 0;
            }
        }

        // recursive brackets handler
        private int BracketsWrapper(List<char> operators, List<int> operands)
        {
           
            int firstBracketIndex = operators.IndexOf('(');
            int leftIterations = 50;
            while (firstBracketIndex >= 0 && leftIterations > 0)
            {
                int deepBracket = 1;
                int bracketCount = 1;
                bool isFindedBracket = false;
                for (int i = firstBracketIndex + 1; i < operators.Count; i++)
                {
                    if (operators[i] == '(')
                    {
                        deepBracket++;
                        bracketCount++;
                    }
                    else if (operators[i] == ')')
                    {
                        deepBracket--;
                        bracketCount++;
                        if (deepBracket == 0)
                        {
                            int innerOperatorCount = i - firstBracketIndex - 1;
                            int innerOperandCount = i - firstBracketIndex - bracketCount + 2;
                            int subResult = BracketsWrapper(
                                operators.GetRange(firstBracketIndex + 1, innerOperatorCount),
                                operands.GetRange(firstBracketIndex, innerOperandCount));
                            operators.RemoveRange(firstBracketIndex, innerOperatorCount + 2);
                            operands.RemoveRange(firstBracketIndex, innerOperandCount);
                            operands.Insert(firstBracketIndex, subResult);
                            isFindedBracket = true;
                            break;
                        }
                    }
                }

                if (firstBracketIndex == 0 &&
                    !isFindedBracket) // if an unpaired bracket hit first, in particular, it corrects an expression of the form (a + b > c + d)
                {
                    operators.RemoveAt(firstBracketIndex);
                }

                firstBracketIndex = operators.IndexOf('(');
                leftIterations--;
            }

            return Calculate(operators, operands);
        }

        private int Calculate(List<char> operators, List<int> operands)
        {
            
            for (int i = operators.Count - 1; i >= 0; i--)
            {
                switch (operators[i])
                {
                    case '*':
                        operands[i] *= operands[i + 1];
                        break;
                    case '/':
                        operands[i] /= operands[i + 1];
                        break;
                    case '%':
                        operands[i] %= operands[i + 1];
                        break;
                    default:
                        continue;
                }
                operators.RemoveAt(i);
                operands.RemoveAt(i + 1);
            }
            int sum = operands[0];
            for (int i = 0; i < operators.Count; i++)
            {
                switch (operators[i])
                {
                    case '+':
                        sum += operands[i + 1];
                        break;
                    case '-':
                        sum -= operands[i + 1];
                        break;
                }
            }
            return sum;
        }
    }
}
