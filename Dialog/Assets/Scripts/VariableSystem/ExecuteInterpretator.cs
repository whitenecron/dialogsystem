using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RPGPack.VariableSystem
{

    /// <summary>
    /// Execute a block of variable assignments (separator ; assignment =).
    /// Based on math interpreter (MathInterpretator). Returns correct execution.
    /// </summary>

    public class ExecuteInterpretator : IInterpretator<bool>
    {
        private VariablesContext _context;
        private IInterpretator<int> _mathInterpretator;

        /// <param name="context"> Context for getting variables </param>
        /// <param name="mathInterpretator">  Interpreter for algebraic expressions  </param>
        public ExecuteInterpretator(VariablesContext context, IInterpretator<int> mathInterpretator)
        {
            _context = context;
            _mathInterpretator = mathInterpretator;
        }

        /// <summary>
        /// Execute block and return correct execution.
        /// </summary>
        /// <param name="block">block assignments for execution</param>
        /// <returns>correct execution</returns>
        public bool Run(string block)
        {
            try
            {
                string[] formulas = block.Split(new char[] {';'}, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < formulas.Length; i++)
                    OneExecute(formulas[i]);
                return true;
            }
            catch
            {
                Debug.LogError("No correct execute block: " + block);
                return false;
            }
        }

        // one assignment
        private void OneExecute(string formula)
        {
            int comparePos = formula.IndexOf('=');
            string leftStr = formula.Substring(0, comparePos).Replace(" ", "");
            int rightValue = _mathInterpretator.Run(formula.Substring(comparePos + 1));
            _context.SetValue(leftStr, rightValue);
            Debug.Log("SetValue " + leftStr + " = " + _context.GetValue(leftStr));
        }
    }
}
