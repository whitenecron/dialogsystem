using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPGPack.VariableSystem
{
    /// <summary>
    /// Calculation/exeption of a block with a return value of type T
    /// </summary>
    public interface IInterpretator<T>
    {
        /// <summary>
        /// Calculation/exeption of a block to type T result
        /// </summary>
        public T Run(string block);
    }
}
