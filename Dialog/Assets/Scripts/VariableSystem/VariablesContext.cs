using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary> Variables, variable contexts, interpretators </summary>
namespace RPGPack.VariableSystem
{

    /// <summary> Storage of variables of a certain context with the main
    /// actions for working with them. Including interpretators. </summary>
    public abstract class VariablesContext
    {
        /// <summary>
        /// Parent context. If the variable is not found in the current context, it is searched
        /// for in the parent; if it is not found in the all parents, it is added to the current one.
        /// </summary>
        public VariablesContext parentContext;

        protected Dictionary<string, int> _variables = new Dictionary<string, int>();
        protected IInterpretator<bool> _executeInterpretator;
        protected IInterpretator<bool> _checkInterpretator;
        protected IInterpretator<int> _mathfInterpretator;

        /// <summary>
        /// Execute variable assignments block 
        /// </summary>
        public void Execute(string block)
        {
            _executeInterpretator.Run(block);
        }
        /// <summary>
        /// Calculate block multiple comparisons to bool result
        /// </summary><returns>comparisons result</returns>
        public bool Check(string condition)
        {
            return _checkInterpretator.Run(condition);
        }

        /// <summary>
        /// Get the value of a variable from the current context or parent contexts
        /// </summary><param name="varName">variable name</param><returns>variable value</returns>
        public int GetValue(string varName)
        {
            if (!_variables.ContainsKey(varName))
            {
                int retValue;
                if (parentContext != null && parentContext.TryGetValue(varName, out retValue))
                    return retValue;
                _variables.Add(varName, 0);
            }

            return _variables[varName];
        }

        /// <summary>
        /// Set the value of a variable from the current context or parent contexts
        /// </summary><param name="varName">variable name</param><param name="value">variable value</param>
        public void SetValue(string varName, int value)
        {
            if (!_variables.ContainsKey(varName))
            {
                if (parentContext == null || !parentContext.TrySetValue(varName, value))
                    _variables.Add(varName, value);
            }
            else
                _variables[varName] = value;
        }

        // Get the value of a variable for parent context (without create new variable if not found)
        private bool TryGetValue(string varName, out int value)
        {
            if (_variables.ContainsKey(varName))
            {
                value = _variables[varName];
                return true;
            }
            if (parentContext!= null)
                return parentContext.TryGetValue(varName, out value);

            value = 0;
            return false;
        }

        // Set the value of a variable for parent context (without create new variable if not found)
        private bool TrySetValue(string varName, int value)
        {

            if (_variables.ContainsKey(varName))
            {
                _variables[varName] = value;
                return true;
            }
            if (parentContext != null)
                return parentContext.TrySetValue(varName, value);

            return false;
        }
    }
}
